// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ASnakeElementBase;
class AActor;
#ifdef HOMEWORK_20_SnakeBase_generated_h
#error "SnakeBase.generated.h already included, missing '#pragma once' in SnakeBase.h"
#endif
#define HOMEWORK_20_SnakeBase_generated_h

#define homework_21_Source_homework_20_SnakeBase_h_23_SPARSE_DATA
#define homework_21_Source_homework_20_SnakeBase_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpeedDown); \
	DECLARE_FUNCTION(execSpeedUp); \
	DECLARE_FUNCTION(execSnakeElementOverlap); \
	DECLARE_FUNCTION(execMove); \
	DECLARE_FUNCTION(execAddSnakeElement);


#define homework_21_Source_homework_20_SnakeBase_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpeedDown); \
	DECLARE_FUNCTION(execSpeedUp); \
	DECLARE_FUNCTION(execSnakeElementOverlap); \
	DECLARE_FUNCTION(execMove); \
	DECLARE_FUNCTION(execAddSnakeElement);


#define homework_21_Source_homework_20_SnakeBase_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeBase(); \
	friend struct Z_Construct_UClass_ASnakeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework_20"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBase)


#define homework_21_Source_homework_20_SnakeBase_h_23_INCLASS \
private: \
	static void StaticRegisterNativesASnakeBase(); \
	friend struct Z_Construct_UClass_ASnakeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework_20"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBase)


#define homework_21_Source_homework_20_SnakeBase_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBase(ASnakeBase&&); \
	NO_API ASnakeBase(const ASnakeBase&); \
public:


#define homework_21_Source_homework_20_SnakeBase_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBase(ASnakeBase&&); \
	NO_API ASnakeBase(const ASnakeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeBase)


#define homework_21_Source_homework_20_SnakeBase_h_23_PRIVATE_PROPERTY_OFFSET
#define homework_21_Source_homework_20_SnakeBase_h_20_PROLOG
#define homework_21_Source_homework_20_SnakeBase_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework_21_Source_homework_20_SnakeBase_h_23_PRIVATE_PROPERTY_OFFSET \
	homework_21_Source_homework_20_SnakeBase_h_23_SPARSE_DATA \
	homework_21_Source_homework_20_SnakeBase_h_23_RPC_WRAPPERS \
	homework_21_Source_homework_20_SnakeBase_h_23_INCLASS \
	homework_21_Source_homework_20_SnakeBase_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define homework_21_Source_homework_20_SnakeBase_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework_21_Source_homework_20_SnakeBase_h_23_PRIVATE_PROPERTY_OFFSET \
	homework_21_Source_homework_20_SnakeBase_h_23_SPARSE_DATA \
	homework_21_Source_homework_20_SnakeBase_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	homework_21_Source_homework_20_SnakeBase_h_23_INCLASS_NO_PURE_DECLS \
	homework_21_Source_homework_20_SnakeBase_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOMEWORK_20_API UClass* StaticClass<class ASnakeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID homework_21_Source_homework_20_SnakeBase_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EMovementDirection::UP) \
	op(EMovementDirection::DOWN) \
	op(EMovementDirection::LEFT) \
	op(EMovementDirection::RIGHT) 

enum class EMovementDirection;
template<> HOMEWORK_20_API UEnum* StaticEnum<EMovementDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
