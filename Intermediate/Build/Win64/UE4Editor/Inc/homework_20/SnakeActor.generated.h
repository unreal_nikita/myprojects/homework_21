// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOMEWORK_20_SnakeActor_generated_h
#error "SnakeActor.generated.h already included, missing '#pragma once' in SnakeActor.h"
#endif
#define HOMEWORK_20_SnakeActor_generated_h

#define homework_21_Source_homework_20_SnakeActor_h_12_SPARSE_DATA
#define homework_21_Source_homework_20_SnakeActor_h_12_RPC_WRAPPERS
#define homework_21_Source_homework_20_SnakeActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define homework_21_Source_homework_20_SnakeActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeActor(); \
	friend struct Z_Construct_UClass_ASnakeActor_Statics; \
public: \
	DECLARE_CLASS(ASnakeActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework_20"), NO_API) \
	DECLARE_SERIALIZER(ASnakeActor)


#define homework_21_Source_homework_20_SnakeActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASnakeActor(); \
	friend struct Z_Construct_UClass_ASnakeActor_Statics; \
public: \
	DECLARE_CLASS(ASnakeActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework_20"), NO_API) \
	DECLARE_SERIALIZER(ASnakeActor)


#define homework_21_Source_homework_20_SnakeActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeActor(ASnakeActor&&); \
	NO_API ASnakeActor(const ASnakeActor&); \
public:


#define homework_21_Source_homework_20_SnakeActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeActor(ASnakeActor&&); \
	NO_API ASnakeActor(const ASnakeActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeActor)


#define homework_21_Source_homework_20_SnakeActor_h_12_PRIVATE_PROPERTY_OFFSET
#define homework_21_Source_homework_20_SnakeActor_h_9_PROLOG
#define homework_21_Source_homework_20_SnakeActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework_21_Source_homework_20_SnakeActor_h_12_PRIVATE_PROPERTY_OFFSET \
	homework_21_Source_homework_20_SnakeActor_h_12_SPARSE_DATA \
	homework_21_Source_homework_20_SnakeActor_h_12_RPC_WRAPPERS \
	homework_21_Source_homework_20_SnakeActor_h_12_INCLASS \
	homework_21_Source_homework_20_SnakeActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define homework_21_Source_homework_20_SnakeActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework_21_Source_homework_20_SnakeActor_h_12_PRIVATE_PROPERTY_OFFSET \
	homework_21_Source_homework_20_SnakeActor_h_12_SPARSE_DATA \
	homework_21_Source_homework_20_SnakeActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	homework_21_Source_homework_20_SnakeActor_h_12_INCLASS_NO_PURE_DECLS \
	homework_21_Source_homework_20_SnakeActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOMEWORK_20_API UClass* StaticClass<class ASnakeActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID homework_21_Source_homework_20_SnakeActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
