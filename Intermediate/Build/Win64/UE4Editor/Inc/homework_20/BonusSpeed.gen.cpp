// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "homework_20/BonusSpeed.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBonusSpeed() {}
// Cross Module References
	HOMEWORK_20_API UClass* Z_Construct_UClass_ABonusSpeed_NoRegister();
	HOMEWORK_20_API UClass* Z_Construct_UClass_ABonusSpeed();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_homework_20();
	HOMEWORK_20_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ABonusSpeed::execSpawnBonus)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnBonus();
		P_NATIVE_END;
	}
	void ABonusSpeed::StaticRegisterNativesABonusSpeed()
	{
		UClass* Class = ABonusSpeed::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SpawnBonus", &ABonusSpeed::execSpawnBonus },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABonusSpeed_SpawnBonus_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABonusSpeed_SpawnBonus_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BonusSpeed.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABonusSpeed_SpawnBonus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABonusSpeed, nullptr, "SpawnBonus", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABonusSpeed_SpawnBonus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABonusSpeed_SpawnBonus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABonusSpeed_SpawnBonus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABonusSpeed_SpawnBonus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABonusSpeed_NoRegister()
	{
		return ABonusSpeed::StaticClass();
	}
	struct Z_Construct_UClass_ABonusSpeed_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusSpeed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABonusSpeed_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_homework_20,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABonusSpeed_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABonusSpeed_SpawnBonus, "SpawnBonus" }, // 1573824153
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonusSpeed_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BonusSpeed.h" },
		{ "ModuleRelativePath", "BonusSpeed.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonusSpeed_Statics::NewProp_BonusSpeed_MetaData[] = {
		{ "Category", "BonusSpeed" },
		{ "ModuleRelativePath", "BonusSpeed.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABonusSpeed_Statics::NewProp_BonusSpeed = { "BonusSpeed", nullptr, (EPropertyFlags)0x0010000000010015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABonusSpeed, BonusSpeed), METADATA_PARAMS(Z_Construct_UClass_ABonusSpeed_Statics::NewProp_BonusSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABonusSpeed_Statics::NewProp_BonusSpeed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABonusSpeed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABonusSpeed_Statics::NewProp_BonusSpeed,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ABonusSpeed_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ABonusSpeed, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABonusSpeed_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABonusSpeed>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABonusSpeed_Statics::ClassParams = {
		&ABonusSpeed::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ABonusSpeed_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ABonusSpeed_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABonusSpeed_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABonusSpeed_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABonusSpeed()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABonusSpeed_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABonusSpeed, 379923711);
	template<> HOMEWORK_20_API UClass* StaticClass<ABonusSpeed>()
	{
		return ABonusSpeed::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABonusSpeed(Z_Construct_UClass_ABonusSpeed, &ABonusSpeed::StaticClass, TEXT("/Script/homework_20"), TEXT("ABonusSpeed"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABonusSpeed);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
