// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "homework_20/SnakeActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSnakeActor() {}
// Cross Module References
	HOMEWORK_20_API UClass* Z_Construct_UClass_ASnakeActor_NoRegister();
	HOMEWORK_20_API UClass* Z_Construct_UClass_ASnakeActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_homework_20();
// End Cross Module References
	void ASnakeActor::StaticRegisterNativesASnakeActor()
	{
	}
	UClass* Z_Construct_UClass_ASnakeActor_NoRegister()
	{
		return ASnakeActor::StaticClass();
	}
	struct Z_Construct_UClass_ASnakeActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASnakeActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_homework_20,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SnakeActor.h" },
		{ "ModuleRelativePath", "SnakeActor.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASnakeActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASnakeActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASnakeActor_Statics::ClassParams = {
		&ASnakeActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASnakeActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASnakeActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASnakeActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASnakeActor, 3805881776);
	template<> HOMEWORK_20_API UClass* StaticClass<ASnakeActor>()
	{
		return ASnakeActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASnakeActor(Z_Construct_UClass_ASnakeActor, &ASnakeActor::StaticClass, TEXT("/Script/homework_20"), TEXT("ASnakeActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASnakeActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
