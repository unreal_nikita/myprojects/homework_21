// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOMEWORK_20_homework_20GameModeBase_generated_h
#error "homework_20GameModeBase.generated.h already included, missing '#pragma once' in homework_20GameModeBase.h"
#endif
#define HOMEWORK_20_homework_20GameModeBase_generated_h

#define homework_21_Source_homework_20_homework_20GameModeBase_h_15_SPARSE_DATA
#define homework_21_Source_homework_20_homework_20GameModeBase_h_15_RPC_WRAPPERS
#define homework_21_Source_homework_20_homework_20GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define homework_21_Source_homework_20_homework_20GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAhomework_20GameModeBase(); \
	friend struct Z_Construct_UClass_Ahomework_20GameModeBase_Statics; \
public: \
	DECLARE_CLASS(Ahomework_20GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework_20"), NO_API) \
	DECLARE_SERIALIZER(Ahomework_20GameModeBase)


#define homework_21_Source_homework_20_homework_20GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAhomework_20GameModeBase(); \
	friend struct Z_Construct_UClass_Ahomework_20GameModeBase_Statics; \
public: \
	DECLARE_CLASS(Ahomework_20GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework_20"), NO_API) \
	DECLARE_SERIALIZER(Ahomework_20GameModeBase)


#define homework_21_Source_homework_20_homework_20GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Ahomework_20GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Ahomework_20GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Ahomework_20GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Ahomework_20GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Ahomework_20GameModeBase(Ahomework_20GameModeBase&&); \
	NO_API Ahomework_20GameModeBase(const Ahomework_20GameModeBase&); \
public:


#define homework_21_Source_homework_20_homework_20GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Ahomework_20GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Ahomework_20GameModeBase(Ahomework_20GameModeBase&&); \
	NO_API Ahomework_20GameModeBase(const Ahomework_20GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Ahomework_20GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Ahomework_20GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Ahomework_20GameModeBase)


#define homework_21_Source_homework_20_homework_20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define homework_21_Source_homework_20_homework_20GameModeBase_h_12_PROLOG
#define homework_21_Source_homework_20_homework_20GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework_21_Source_homework_20_homework_20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	homework_21_Source_homework_20_homework_20GameModeBase_h_15_SPARSE_DATA \
	homework_21_Source_homework_20_homework_20GameModeBase_h_15_RPC_WRAPPERS \
	homework_21_Source_homework_20_homework_20GameModeBase_h_15_INCLASS \
	homework_21_Source_homework_20_homework_20GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define homework_21_Source_homework_20_homework_20GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework_21_Source_homework_20_homework_20GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	homework_21_Source_homework_20_homework_20GameModeBase_h_15_SPARSE_DATA \
	homework_21_Source_homework_20_homework_20GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	homework_21_Source_homework_20_homework_20GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	homework_21_Source_homework_20_homework_20GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOMEWORK_20_API UClass* StaticClass<class Ahomework_20GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID homework_21_Source_homework_20_homework_20GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
