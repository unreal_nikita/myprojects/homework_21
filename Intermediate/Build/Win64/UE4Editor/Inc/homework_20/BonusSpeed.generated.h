// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HOMEWORK_20_BonusSpeed_generated_h
#error "BonusSpeed.generated.h already included, missing '#pragma once' in BonusSpeed.h"
#endif
#define HOMEWORK_20_BonusSpeed_generated_h

#define homework_21_Source_homework_20_BonusSpeed_h_13_SPARSE_DATA
#define homework_21_Source_homework_20_BonusSpeed_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnBonus);


#define homework_21_Source_homework_20_BonusSpeed_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnBonus);


#define homework_21_Source_homework_20_BonusSpeed_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABonusSpeed(); \
	friend struct Z_Construct_UClass_ABonusSpeed_Statics; \
public: \
	DECLARE_CLASS(ABonusSpeed, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework_20"), NO_API) \
	DECLARE_SERIALIZER(ABonusSpeed) \
	virtual UObject* _getUObject() const override { return const_cast<ABonusSpeed*>(this); }


#define homework_21_Source_homework_20_BonusSpeed_h_13_INCLASS \
private: \
	static void StaticRegisterNativesABonusSpeed(); \
	friend struct Z_Construct_UClass_ABonusSpeed_Statics; \
public: \
	DECLARE_CLASS(ABonusSpeed, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/homework_20"), NO_API) \
	DECLARE_SERIALIZER(ABonusSpeed) \
	virtual UObject* _getUObject() const override { return const_cast<ABonusSpeed*>(this); }


#define homework_21_Source_homework_20_BonusSpeed_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABonusSpeed(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABonusSpeed) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABonusSpeed); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABonusSpeed); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABonusSpeed(ABonusSpeed&&); \
	NO_API ABonusSpeed(const ABonusSpeed&); \
public:


#define homework_21_Source_homework_20_BonusSpeed_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABonusSpeed(ABonusSpeed&&); \
	NO_API ABonusSpeed(const ABonusSpeed&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABonusSpeed); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABonusSpeed); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABonusSpeed)


#define homework_21_Source_homework_20_BonusSpeed_h_13_PRIVATE_PROPERTY_OFFSET
#define homework_21_Source_homework_20_BonusSpeed_h_10_PROLOG
#define homework_21_Source_homework_20_BonusSpeed_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework_21_Source_homework_20_BonusSpeed_h_13_PRIVATE_PROPERTY_OFFSET \
	homework_21_Source_homework_20_BonusSpeed_h_13_SPARSE_DATA \
	homework_21_Source_homework_20_BonusSpeed_h_13_RPC_WRAPPERS \
	homework_21_Source_homework_20_BonusSpeed_h_13_INCLASS \
	homework_21_Source_homework_20_BonusSpeed_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define homework_21_Source_homework_20_BonusSpeed_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	homework_21_Source_homework_20_BonusSpeed_h_13_PRIVATE_PROPERTY_OFFSET \
	homework_21_Source_homework_20_BonusSpeed_h_13_SPARSE_DATA \
	homework_21_Source_homework_20_BonusSpeed_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	homework_21_Source_homework_20_BonusSpeed_h_13_INCLASS_NO_PURE_DECLS \
	homework_21_Source_homework_20_BonusSpeed_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HOMEWORK_20_API UClass* StaticClass<class ABonusSpeed>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID homework_21_Source_homework_20_BonusSpeed_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
