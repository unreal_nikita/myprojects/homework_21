// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "homework_20GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMEWORK_20_API Ahomework_20GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
