// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	SpawnFood();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);

		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Destroy();
		}
	}
}

void AFood::SpawnFood()
{
	FVector NewPoint(0.f, 0.f, 0.f);

	NewPoint.X = FMath::FRandRange(-1200.f, 1200.f);
	NewPoint.Y = FMath::FRandRange(-1200.f, 1200.f);

	this->SetActorLocation(NewPoint);

}

