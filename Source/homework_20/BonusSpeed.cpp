// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusSpeed.h"
#include "SnakeBase.h"

// Sets default values
ABonusSpeed::ABonusSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BonusSpeed = 0.05;	
}

// Called when the game starts or when spawned
void ABonusSpeed::BeginPlay()
{
	Super::BeginPlay();
	SpawnBonus();
}

// Called every frame
void ABonusSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABonusSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);

		if (IsValid(Snake))
		{
			Snake->SpeedUp(BonusSpeed);
			Destroy();
		}
	}
}

void ABonusSpeed::SpawnBonus()
{
	FVector NewPoint(0.f, 0.f, 0.f);

	NewPoint.X = FMath::FRandRange(-1200.f, 1200.f);
	NewPoint.Y = FMath::FRandRange(-1200.f, 1200.f);
	
	this->SetActorLocation(NewPoint);
}


