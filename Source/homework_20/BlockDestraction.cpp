// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockDestraction.h"
#include "SnakeBase.h"

// Sets default values
ABlockDestraction::ABlockDestraction()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABlockDestraction::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlockDestraction::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlockDestraction::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);

		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}

