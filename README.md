# Snake

Developed with Unreal Engine 4

## Description

Made a simple snake game to work in Unreal with C++

![Screenshot](Images/Screenshot.png)

Snake move and eat balls and some balls make the snake slower and some faster. If the snake hits an obstacle, it dies and stops.

